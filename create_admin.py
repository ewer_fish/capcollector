import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "CAPCollector.settings")
import logging
import django


django.setup()

from django.contrib.auth.models import User

user_name = os.getenv('ADMIN_USERNAME', 'admin')
user_email = os.getenv('ADMIN_EMAIL', 'admin@example.com')
user_password = os.getenv('ADMIN_PASSWORD', 'password123')


if User.objects.filter(username = user_name).exists() or User.objects.filter(email = user_email).exists():
    logging.warn('User already exists')
else:
    User.objects.create_superuser(user_name, user_email, user_password)

# Base Dockerfile for CAPTools project.
# See https://docs.docker.com/reference/builder/ for configuration references.
# See https://docs.docker.com/#installation-guides for Docker installation.

FROM python:2.7.18-slim
ENV PYTHONUNBUFFERED=1

RUN mkdir /var/app
# https://www.educative.io/edpresso/what-is-the-workdir-command-in-docker
WORKDIR /var/app

# Copy packages file.
ADD packages.txt /var/app

# Install required packages.
RUN apt-get -q update ; \
    apt-get upgrade -y ; \
    apt-get install -y -q gcc g++ ; \
    apt-get install -y -q $(grep -vE "^\s*#" packages.txt | tr "\n" " ") && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/*

# Copy requirements file.
ADD requirements.txt /var/app

# update pip to the latest supported verion
RUN pip install -U pip

# Resolve Python modules dependencies.
RUN pip install -r requirements.txt

# Copy application source files.
ADD . /var/app

RUN ln -s /var/app/client /var/app/static

# Port to expose
EXPOSE 8080

CMD ["/var/app/runserver.sh"]

#!/bin/bash
export PYTHONPATH="/var/app;$PYTHONPATH"
SITE_PORT="${SITE_PORT:-8080}"


cd /var/app

# calculate 
python manage.py makemigrations

# update the database with changes
python manage.py migrate --noinput

# create admin users
python create_admin.py

# Compile translation messages to .mo files.
python manage.py compilemessages

# Collect static files.
python manage.py collectstatic -v0 --noinput

# Start Server
python manage.py runserver 0.0.0.0:$SITE_PORT

#!/bin/bash

if test -f "requirements.txt"; then
    BASE_DIR=$(pwd)
else
    BASE_DIR=$(realpath ..)
fi

docker build \
    -t capcollector \
    --network host \
    $BASE_DIR

#!/bin/bash

if test -f "requirements.txt"; then
    BASE_DIR=$(pwd)
else
    BASE_DIR=$(realpath ..)
fi

cd $BASE_DIR && \
pip install -r requirements.txt && \

python manage.py makemigrations && \

python manage.py migrate --noinput && \

python create_admin.py && \

python manage.py compilemessages && \

python manage.py collectstatic -v0 --noinput && \

echo "Installation completed attempting to run application: " && \

python manage.py runserver 0.0.0.0:8080

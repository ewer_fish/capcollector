CAPCollector&trade;
============
[![Build Status](https://travis-ci.org/CAPTools/CAPCollector.svg?branch=master)](https://travis-ci.org/CAPTools/CAPCollector)


**CAPCollector&trade;** is a server designed to authenticate, sign, aggregate
and forward [Common Alerting Protocol (v1.2)](http://docs.oasis-open.org/emergency/cap/v1.2/CAP-v1.2-os.html)
alerts.

Combined with [CapCreator&trade;](https://github.com/CAPTools/CapCreator),
the tools offer an easy-to-use web form for creating and updating alerts,
authentication mechanisms to control alert publishing, a database for recording
published alerts, and endpoints for external users to view and download active
alerts.

It is designed to run on any standard web server, both locally hosted and from
cloud-hosted providers.  See the [documentation](./documentation) folder for
installation instructions.

**Features**

 * Create and reuse standard templates to simplify new alert generation.
 * Required technical fields in the Common Alerting Protocol specification are
 populated automatically to ensure alerts are properly and consistently identified
 and updated.
 * Authentication gives you control over who can officially publish alerts.
 * Built-in support for a serving a feed of active alerts, both as XML and as an
 embeddable HTML widget.
 * Designed to work well on tablets and mobile devices as well as desktops.


**A few notable limits on the current version:**

 * Only one language can be used in a single message (however multiple messages
 can be authored to address multilingual alerting requirements);
 * Only one target area can be specified for an individual alert message (but
 it may include multiple polygons and/or circles); and,
 * All alerts are assumed to be effective immediately; the "effective" and
 "onset" elements are not supported.

The CAP Creator combines two pieces of the [open-source CAPTools™](https://github.com/CAPTools)
project originally created at Carnegie Mellon University by a team lead by Art
Botterell, one of the original designers of the Common Alerting Protocol.

 **CAPTools&trade;**, **CAPCreator&trade;**, **CAPCollector&trade;** and
 **CAPConsumer&trade;** are trademarks of Carnegie Mellon  University.

**Modifications**
The CAPCollector Tools was modified from the original to support the requirements of the Caribbean's Fisheries Early Warning and Emergency Response system. Which focuses on providing CAP compatible alerts to fisheries specific target communities.

**Additional Installation** 

### Update for 2021
NB: The CapCollector uses a very old version of Django and only supports Python 2.7 therefore, you need a virtual environment that supports the python 2. 


### Development
#### Installation
1. Download and Install [docker](https://www.docker.com/products/docker-desktop)
2. Ensure docker compose is available
```bash
docker compose help
```
3. Run and build CAP server
```bash
docker compose -f docker-compose.yaml up --build
```

##### Notes:
* The recommended approach is to use docker for development, however there is a `setup.sh` 
  file in the `scripts` folder that can be used for local development. 
  This file mirrors the actions performed in building the docker container
* We used multiple docker files to distinguish between the development and production
  environment. If docker compose command are executed without the `-f` flag, then the system will use both
  the `docker-compose.yaml` and `docker-compose.override.yaml` which will utilize the 
  development values.

#### Run tests
You can also run the functional tests included with the code to ensure the
application is functioning correctly.

```bash
docker compose -f docker-compose.yaml run --rm web python manage.py test
```


To rerun a single test that may have been failing
```bash
python manage.py test tests.test_utils
python manage.py test tests.test_utils.UtilsTests.test_parse_valid_alert
```
**NB: Ensure that you place the docker compose command before calling the test commands above**

If you are using docker as the development environment you will need to remember to rebuild for every change that you make.
Usually subsequent builds should be fast due to Docker's caching of the build steps. An example of a command used for a specific test:

```bash
docker compose -f docker-compose.yaml build && docker compose -f docker-compose.yaml run --rm web python manage.py test tests.test_utils.UtilsTests.test_create_alert_created -v 2
```

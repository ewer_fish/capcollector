"""CAPCollector project production settings."""
# -*- coding: utf-8 -*-
from sensitive import *


###### Standard configuration.  You need to set these variables.  ######

# Change to http at your own risk.
SITE_SCHEME = ENV_SITE_SCHEME

# Change to the URL where users will find this app.
# If you using Appengine, put <application_id>.appspot.com (e.g.
# cap-tools.appspot.com)
SITE_DOMAIN = ENV_SITE_DOMAIN

# A string representing the time zone for this installation.
# https://docs.djangoproject.com/en/dev/ref/settings/#time-zone
TIME_ZONE = ENV_TIME_ZONE

# Default language.
# The exact code must also appear in LANGUAGES below.
# See https://docs.djangoproject.com/en/dev/ref/settings/#language-code.
LANGUAGE_CODE = ENV_LANGUAGE_CODE

# Add new languages here.
# The list is a tuple of two-tuples in the format (language code, language name)
# Both the language and the country parts of language code are lower case.
# The separator is a dash. Examples: it, de-at, es, pt-br.
# See https://docs.djangoproject.com/en/dev/ref/settings/#languages
# and https://docs.djangoproject.com/en/1.7/topics/i18n/#definitions for more
# information.
LANGUAGES = (
    ("en-us", "English"),
    ("hi", "Chinese"),
    ("pt-br", "Portuguese"),
)

# Set map default viewport here.
MAP_DEFAULT_VIEWPORT = {
    "center_lat": ENV_CENTER_LAT,
    "center_lon": ENV_CENTER_LON,
    "zoom_level": ENV_ZOOM_LEVEL,
}

# A dictionary containing the settings for all databases to be used with Django.
# https://docs.djangoproject.com/en/dev/ref/settings/#databases
# See https://cloud.google.com/appengine/docs/python/cloud-sql/django for
# Django on CloudSQL documentation.
# DATABASE_* variables should be defined in sensitive.py module.
DATABASES = {
    "default": {
        "ENGINE": DATABASE_ENGINE,
        # "HOST": DATABASE_HOST,  # /cloudsql/... path or regular IP address.
        "NAME": DATABASE_NAME,
        # "USER": DATABASE_USER,
        # "PASSWORD": DATABASE_PASSWORD,
    },
}

# Default Alert expiration duration. Set to "Other" if want to input duration
# manually.
DEFAULT_EXPIRES_DURATION_MINUTES = 60

# A boolean that specifies whether to show datetime calender picker when
# "Other" option is selected during "Alert expiration" specification.
# If false, can input expiration duration in minutes.
USE_DATETIME_PICKER_FOR_EXPIRES = False

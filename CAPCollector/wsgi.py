"""WSGI config for CAPCollector project.

It exposes the WSGI callable as a module-level variable named "application".

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os
import sys
import site


# AppEngine third-party apps path include.
# if "SERVER_SOFTWARE" in os.environ:
#   if os.environ.get("SERVER_SOFTWARE"):  # AppEngine.
#     sys.path.insert(
#         0, os.path.join(os.path.dirname(os.path.dirname(__file__)), "libs"))
#   else:  # AppEngine managed VMs.
#     sys.path.insert(0, "/usr/local/lib/python2.7/dist-packages/")
# else:  # We running on a regular OS (based on previous installation instructions)
#     app_path = os.path.dirname(os.path.dirname(__file__))
#     venv_path = os.path.join(os.path.dirname(app_path), "venv")  # venv should be in ../venv
#     # Add the site-packages of the chosen virtualenv to work with
#     site.addsitedir(os.path.join(venv_path, '/lib/python2.7/site-packages'))
#     # Add the app's directory to the PYTHONPATH
#     sys.path.append(app_path)
#     # Activate your virtual environment
#     activate_env="{0}/bin/activate_this.py".format(venv_path)
#     execfile(activate_env, dict(__file__=activate_env))
#     # used for debugging
#     print("app {0}, venv {1}".format(app_path,venv_path))
#     print("Activate env {0}".format(activate_env))

from django.core.wsgi import get_wsgi_application
from whitenoise.django import DjangoWhiteNoise

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "CAPCollector.settings")
application = get_wsgi_application()
application = DjangoWhiteNoise(application) 

import os

SECRET_KEY = os.getenv('SECRET_KEY', '@l@24ziuxky!8sh+yiq5ot#d^d4fqyxe$f39f@(ye183lq#hkf')

# Database configurations
DATABASE_ENGINE = os.getenv('DATABASE_ENGINE', "django.db.backends.sqlite3")  # "django.db.backends.mysql" for mysql
DATABASE_HOST = os.getenv('DATABASE_HOST', '')
DATABASE_NAME = os.getenv('DATABASE_NAME', 'db.sqlite3')
DATABASE_USER = os.getenv('DATABASE_USER', '')
DATABASE_PASSWORD = os.getenv('DATABASE_PASSWORD', '')

# Site Settings
ENV_SITE_DOMAIN = os.getenv('SITE_DOMAIN', "myagency.gov")
ENV_SITE_SCHEME = os.getenv('SITE_SCHEME', "https")

# System Settings
ENV_TIME_ZONE = os.getenv('TIMEZONE', "US/Central")
ENV_LANGUAGE_CODE = os.getenv('LANGUAGE_CODE', "en-us")

# Set map default viewport here.
ENV_CENTER_LAT = os.getenv('CENTER_LAT', 37.422)
ENV_CENTER_LON = os.getenv('CENTER_LON', -122.084)
ENV_ZOOM_LEVEL = os.getenv('ZOOM_LEVEL', 12)

# Flags for testing against a development MySQL database. This is used by default with AppEngine's dev_appserver,
# and can also be turned on by running 'export CAP_TOOLS_DB=dev', but this is not required if using Django's internal sqlite db.
DEV_DATABASE_HOST = os.getenv('DEV_DATABASE_HOST')
DEV_DATABASE_NAME = os.getenv('DEV_DATABASE_NAME')
DEV_DATABASE_USER = os.getenv('DEV_DATABASE_USER')
DEV_DATABASE_PASSWORD = os.getenv('DEV_DATABASE_PASSWORD')
